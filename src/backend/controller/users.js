var mongoose = require("mongoose");
var users = require("../model/user");
var jwt = require("jsonwebtoken");
var config = require('../config/config');
var nodemailer = require('nodemailer');
var rand = require("random-key");
/**
This function gets request from frontend and checks if user is already a registered or not
if not then it will create new user
**/
exports.create = function(req, res) {

    var user = new users();
    /**
    This code will validate the fields
    **/
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email does not appear to be valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password', 'Password length should be minimum 8 digit').len(8, 15);
    req.checkBody('fullName', 'Name is required').notEmpty();
    req.checkBody('userType', 'Name is required').notEmpty();
    req.checkBody('mobileNo', 'Mobile number is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        console.log(errors);
        res.json({
            success: false,
            message: 'Invalid parameters',
            data: errors
        });
    } else {
        /**
        it checks if user exists or not
        **/
        users.findOne({
            email: req.body.email
        }, function(err, usr) {
            if (usr) {
                res.json({
                    success: false,
                    message: 'User is already a member',
                    data: null
                });
            } else {
                user.email = req.body.email;
                user.password = user.generateHash(req.body.password);
                user.fullName = req.body.fullName;
                user.userType = req.body.userType;
                user.mobileNo = req.body.mobileNo;
                console.log(user);
                user.save(function(err) {
                    if (err) {
                        var msg = '';
                        if (err.errors.email)
                            msg = err.errors.email.path + ' is ' + err.errors.email.kind;
                        else if (err.errors.password)
                            msg = err.errors.password.path + ' is ' + err.errors.password.kind;
                        else if (err.errors.fullName)
                            msg = err.errors.fullName.path + ' is ' + err.errors.fullName.kind;
                        else if (err.errors.userType)
                            msg = err.errors.userType.path + ' is ' + err.errors.userType.kind;
                        else if (err.errors.mobileNo)
                            msg = err.errors.mobileNo.path + ' is ' + err.errors.mobileNo.kind;
                        res.status(500).send(msg);
                    } else {
                        res.json({
                            success: true,
                            message: 'User is successfully added',
                            data: user
                        });
                    }
                });
            }
        });

    }

}

/**
This function will authenticate the users
**/
exports.login = function(req, res, next) {
    users.findOne({
        email: req.body.email
    }, function(err, usr) {
        if (err) throw err;
        if (!usr) {
            res.json({
                success: false,
                message: 'User not found',
                data: null
            });
        } else {
            if (!usr.validPassword(req.body.password)) {
                res.json({
                    success: false,
                    message: 'Password is wrong.',
                    data: null
                });
            } else {
                usr.password = req.body.password;
                var token = jwt.sign(usr, config.secret, {
                    expiresIn: 1440 * 60 * 30 // expires in 1440 minutes
                });
                res.json({
                    success: true,
                    message: 'You are logged in and Enjoy your token!',
                    data: token,
                    user:usr.fullName,
                    userType:usr.userType,
                    userId:usr.userId
                });
            }
        }
    })
};

/**
This function lists all the user.
**/
exports.list = function(req, res) {
    users.find({userType:"carer"}, function(err, carers) {
        res.json({
            success: true,
            message: 'List of carer',
            data: carers
        });
    });
};
