var wamp = require('./wamp_session');
var autobahn = require('autobahn');
var connection = new autobahn.Connection({
   url: 'ws://localhost:8080/ws',
   realm: 'realm1'
});
connection.onopen = function (session) {
   session.log("Session open.");
   wamp.setSession(session);
   wamp.getSession().publish('app.carenet.heartbeat',['Socket has been started']);
   function onhello (args) {
      console.log("event received: " + args[0]);
   }
   wamp.getSession().subscribe('app.carenet.route', onhello).then(
      function (sub) {
         console.log("subscribed to topic Route");
      },
      function (err) {
         console.log("failed to subscribed: " + err);
         console.log(err);
      }
   );
};
connection.onclose = function (reason, details) {
   console.log("WAMP connection closed", reason, details);
   var session = null;
   wamp.setSession(session);
}
connection.open();

