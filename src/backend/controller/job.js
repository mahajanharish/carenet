var jobs = require("../model/job");
var wamp = require('./wamp_session');
var users = require("../model/user");
var carersjob = require("../model/carersjob");
var async = require("async");
// Our Local Credentials
// var accountSid = 'AC177af2e2f4c05ea4da89a5f4f7a31962';
// var authToken = 'dee683d9f28410115c11766617f23e9e';

// Test 
// var accountSid = 'AC48844b8015b26bde79ed984f2db79bee';
// var authToken = 'f0dae5581d15da33f8aece668c349c33';

// Live
var accountSid = 'AC4588d0d961b675a7b59a251eaecf60b5';
var authToken = '094352eac5b9118e29195ebd25b05519';

var twilio = require('twilio');
var client = new twilio.RestClient(accountSid, authToken);

// Joblogs variable
var jobCreatelog = "Job created";
var jobAlreadyadded = "Job already added";
var assignNewjob = "New job offer from CareNet";
var sender = "+61428857513";
var jobSentto = "Job sent to ";
var jobAssignedtocarers = "Job assigned to carers";
var noJobfound = 'No job found';
var jobAcceptedby = "Job accepted by ";
var internalError = "Internal Error: ";
var notesAddedtojob = "Notes added to job";
var jobDeclinedby = "Job declined by ";
var jobStartedby = "Job started by ";
var jobStppedby = "Job stopped by ";
var jobCompleted = "Job Completed";
var jobDeleted = "Job Deleted";
var listOfjob = "List of jobs";
// End joblogs variabl

/**
This function gets request from frontend and creates job.
**/
exports.create = function(req, res) {
        var datetime = new Date();
        var job = new jobs();
        jobs.findOne({
            clientName: req.body.clientName,
            gender: req.body.gender,
            dob: req.body.dob,
            address: req.body.address,
            telephone: req.body.telephone,
            nok: req.body.nok,
            careNeeds: req.body.careNeeds,
            jobDate: datetime,
            jobStatus: req.body.jobStatus,
            flag: req.body.flag
        }, function(err, carers) {
            if (carers) {
                res.json({
                    success: false,
                    message: jobAlreadyadded,
                    data: null
                });
            } else {
                job.clientName = req.body.clientName,
                    job.jobLog = {
                        'message' : jobCreatelog,
                        'time': datetime
                    },
                    job.gender = req.body.gender,
                    job.dob = req.body.dob,
                    job.address = req.body.address,
                    job.telephone = req.body.telephone,
                    job.nok = req.body.nok,
                    job.careNeeds = req.body.careNeeds,
                    job.jobDate = datetime,
                    job.flag = "Unallocated",
                    job.jobStatus = req.body.jobStatus,
                    job.alerts = req.body.alerts,
                    job.date_and_time_care_episode_required = req.body.date_and_time_care_episode_required,
                    job.jobDetails = req.body.jobDetails
                    console.log(job);
                    
                    job.save(function(err) {
                        if (err) {
                            console.log(err);
                            var msg = '';
                            if (err.errors.clientName)
                                msg = err.errors.clientName.path + ' is ' + err.errors.clientName.kind;
                            else if (err.errors.gender)
                                msg = err.errors.gender.path + ' is ' + err.errors.gender.kind;
                            // else if (err.errors.dob)
                            //     msg = err.errors.dob.path + ' is ' + err.errors.dob.kind;
                            else if (err.errors.address)
                                msg = err.errors.address.path + ' is ' + err.errors.address.kind;
                            else if (err.errors.telephone)
                                msg = err.errors.telephone.path + ' is ' + err.errors.telephone.kind;
                            else if (err.errors.nok)
                                msg = err.errors.nok.path + ' is ' + err.errors.nok.kind;
                            else if (err.errors.careNeeds)
                                msg = err.errors.careNeeds.path + ' is ' + err.errors.careNeeds.kind;
                            else if (err.errors.flag)
                                msg = err.errors.flag.path + ' is ' + err.errors.flag.kind;
                            res.status(500).send(msg);
                        } else {
                        // wamp.getSession().publish('app.carenet.jobcreated',['Socket has been started']);
                            res.json({
                                success: true,
                                message: jobCreatelog,
                                data: job
                            });
                        }
                    });
                }
        });
    }

/**
This function lists all the jobs.
**/
exports.list = function(req, res) {
    jobs.find({$query: {},$orderby: { flag : -1, jobDate: 1, clientName:1 }}, function(err, list) {
        if(err)
        {
            res.json({
                success: true,
                message: internalError,
                data: err
            });

        }
        else
        {
            res.json({
                success: true,
                message: listOfjob,
                data: list
            });
        }
    });
};


exports.selectedcarerlist = function(req, res){
       jobs.findOne({
            jobNumber: {$in : req.body.jobNumber}
        }, function(err, list) {
        if(err)
        {
            res.json({
                success: true,
                message: internalError,
                data: err
            });

        }
        else
        {
            res.json({
                success: true,
                message: listOfjob,
                data: list
            });
        }
    });
    
};

/**
This function will update job logs
**/
exports.update = function(req, res) {
    var datetime = new Date();
    jobs.update({
        jobNumber: req.body.jobNumber
    }, {
        $push: {
            jobLog: {
                "message": req.body.message,
                "time": datetime
            }
        }
    }, function(err, data) {
        if (err) {
            res.json({
                success: false,
                message: 'Joblog not updated',
                data: err
            });
        } else {
            res.json({
                success: true,
                message: 'Joblog updated',
                data: data
            });
        }
    });
};
/**
This function accepts parameters of particular job and returns its history
**/
exports.history = function(req, res) {
    jobs.findOne({
        jobNumber: {$in : req.body.jobNumber}
    }, function(err, data) {
        if (err) {
            res.json({
                success: false,
                message: noJobfound,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: 'Job history',
                data: data
            });
        }
    });
};

function notifyUser(mobileNo, carerNo, channel, msg, callback) {
 
  //console.log("Event publish to " + channel);
  // wamp publish event to refresh joblist data on admin side/ viewer side
  // need to subscribe app.carenet.joblist channel by admin/viewer
  wamp.getSession().publish(channel, [msg]);
  // mobile message
  if(mobileNo)
  {
    client.messages.create({
        body: assignNewjob,
        to: mobileNo,
        from: sender
    }, function(err, message) {
        console.log(err);
        console.log(message);
        if (err) {
        }
        callback(true);
    });
  }
  else
  {
        callback(true);
  }
}

// This function is use to assign job to carers
exports.jobToCarer = function(req, res) {
  var datetime = new Date();
  var arrayCarer = req.body.carerNo;
  var carerCount = 0;
  var carerNames = '';

    async.eachSeries(arrayCarer, function(carerNo, cbCarer) {
    carerCount++;
    //console.log(arrayCarer);
    // arrayCarer.forEach(function(carerNo) {
    users.findOne({
      userId: carerNo
    }, function(err, usr) {
      if (err) {
        // res.json({
        //   success: false,
        //   message: 'There is no carer of this ID',
        //   data: err
        // });
      } else {
        jobs.update({
          jobNumber: req.body.jobNumber
        }, {
          $set: {
            "jobStatus": "Sent"
          },
          $push: {
            jobLog: {
              "message": jobSentto + usr.fullName,
              "time": datetime
            },
             sentCarer: {
               "carerNo": carerNo
               //"carerJobstatus": req.body.carerJobstatus
             }
          }
        }, function(err, data) {
          if (err) {

            // possible can't set header error : remember

            // res.json({
            //   success: false,
            //   message: 'job is not assigned to carer',
            //   data: err
            // });

          } else {
            // function for sending  sms as well as socket events
            /**
            Purpose: To insert the assigned jobs to carer.
            Created Date: 8th Dec 2016 4:46 PM
            Created  By: Harish Mahajan
            **/
            carersjob.update({
              carerno: carerNo
            }, {
              $push: {
                jobno: req.body.jobNumber
              }
            }, {
              upsert: true,
              safe: false
            }, function(err, data) {
              if (err) {
                // need propar error handling
                // res.json({
                //   success: false,
                //   message: 'job is not assigned to carer',
                //   data: err
                // });
              } else {
                  jobs.update({  
                         jobNumber: req.body.jobNumber, 
                        "sentCarer.carerNo" : ""+req.body.carerNo+"",      
                        }, {
                            $set:{ 
                                'sentCarer.$.carerJobstatus': 'sent'
                            }
                },function(err, data) {
                    if(err)
                    {
                        res.json({
                                success: false,
                                message: internalError,
                                data: err
                        });
                    }
                    else
                    {
                        var channel = "app.carenet.jobs." + carerNo;  
                        var msg = req.body.jobNumber;
                        notifyUser(usr.mobileNo, carerNo, channel, msg , function(status) {
                        if (status) {
                            if (carerCount == arrayCarer.length) {
                            res.json({
                                success: true,
                                message: jobSentto + usr.fullName,
                                data: null
                            });
                            } else
                            cbCarer();
                        }
                        });
                    }
                } );
               
              }
            }); // carer update

          }

        }); // job update

      }

    }); // user find

  }); // async end
  // you are going to get error saomething like this :
  // Caught exception: Error: Can't set headers after they are sent.
  // Error: Can't set headers after they are sent.
  // res.json({
  //   success: true,
  //   message: 'job is assigned to carer',
  //   data: null
  // });

};

/**
This function lists all the jobs that are assigned to particular carer.
**/
exports.listJob = function(req, res) {
    jobs.find({
        carerNo: req.body.carerNo
    }, function(err, list) {
        if (err) {
            res.json({
                success: false,
                message: noJobfound,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: listOfjob,
                data: list
            });
        }

    });
};

/**
This function will set the carer number with job number.
**/
exports.acceptJob = function(req, res) {
    //console.log("userName : ",req.body.userName);
    var datetime = new Date();
    jobs.update({
        jobNumber: { $in : req.body.jobNumber} 
    }, {
        $set: {
            jobAssigned: req.body.carerNo
            
        },
         $push: {
             jobLog : {
                        'message': jobAcceptedby + req.body.userName,
                        'time': datetime
                  }           
            },
        jobStatus : "accept"
    }, function(err, job) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
                //console.log("req.body.jobNumber",req.body.jobNumber);
                //console.log("req.body.carerNo",req.body.carerNo);
                jobs.update({  
                         jobNumber: req.body.jobNumber, 
                        "sentCarer.carerNo" : ""+req.body.carerNo+"",      
                        }, {
                            $set:{ 
                                'sentCarer.$.carerJobstatus': 'accepted'
                            }
                },function(err, data) {
                    if(err)
                    {
                        res.json({
                                success: false,
                                message: internalError,
                                data: err
                        });
                    }
                    else
                    {
                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has ACCEPTED Job #' + req.body.jobNumber]);
                        res.json({
                            success: true,
                            message: jobAcceptedby + req.body.userName,
                            data: job
                        });

                    }
                } );
        }
    });
};

/**
This function returns all the jobs that a specific carer have.
**/

exports.carerjoblist = function(req, res) {
    jobs.find({
        jobAssigned: req.body.carerNo
    }, function(err, list) {
        if (err) {
            res.json({
                success: false,
                message: noJobfound,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: listOfjob,
                data: list
            });
        }

    });
};

/**
This function will update job with notes that carer has sent after accepting job.
**/
exports.jobNote = function(req, res) {
    jobs.update({
        jobNumber: req.body.jobNumber
    }, {
        $set: {
            notes: req.body.notes
        }
    }, function(err, job) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: notesAddedtojob,
                data: job
            });
        }

    });
};



/**
This function will set the carer numbers who have rejected the particular job.
**/
exports.rejectJob = function(req, res) {
    var datetime = new Date();
    jobs.update({
        jobNumber: { $in : req.body.jobNumber }
    }, {
        $push: {
            rejectedCarer: {
                "carerNo": req.body.carerNo
            },
            jobLog : {
                    'message': jobDeclinedby + req.body.userName,
                    'time': datetime
            }
        },
        jobStatus : "decline"
    }, function(err, job) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
            jobs.update({  
                         jobNumber: req.body.jobNumber, 
                        "sentCarer.carerNo" : ""+req.body.carerNo+"",      
                        }, {
                            $set:{ 
                                'sentCarer.$.carerJobstatus': 'declined'
                            }
                },
                function(err, data) {
                    if(err)
                    {
                        res.json({
                            success: false,
                            message: internalError,
                            data: err
                        });
                    }   
                    else{
                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has DECLINED Job #' + req.body.jobNumber]);
                        res.json({
                            success: true,
                            message: jobDeclinedby,
                            data: job
                        });     
                    }                
                });
        }

    });
};

/**
Method Name: updateAction
Purpose: Updating Job Status to Start and Stop
Created Date: 29th Nov., 2016
Created  By: Hardik Thakor
**/



exports.updateAction = function(req, res) {
    var datetime = new Date();
    var msglog='';
    if(req.body.status=='start')
        msglog= jobStartedby + req.body.userName;
    else
        msglog= jobStppedby + req.body.userName;
    var datetime = new Date();
    jobs.update({ 
            jobNumber: { $in:req.body.jobNumber} 
    },{
        $set: { 
            jobStatus : req.body.status
           
    },  
        $push: {
            jobLog : {
                    'message': msglog,
                    'time': datetime
            }
        }    
    }, function(err, job) {
                  if (err) {
                    res.json({
                      success: false,
                      message: internalError + req.body.jobNumber,
                      data: err
                    });
                  } else {
                      if(req.body.status=='start')
                      {
                           jobs.update({  
                                    jobNumber: req.body.jobNumber, 
                                    "sentCarer.carerNo" : ""+req.body.carerNo+"",      
                                    }, {
                                        $set:{ 
                                            'sentCarer.$.carerJobstatus': 'started'
                                        }
                            },
                            function(err, data) {
                                if(err)
                                {
                                    res.json({
                                        success: false,
                                        message: internalError,
                                        data: err
                                    });
                                }   
                                else{
                                    if(req.body.status=='start')
                                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has STARTED Job #' + req.body.jobNumber]);
                                    else
                                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has STOPPED Job #' + req.body.jobNumber]);
                                    res.json({
                                    success: true,
                                    message: req.body.jobNumber +'',
                                    data: job
                                    });    
                                }                
                            });
                      }
                      else
                      {
                          console.log("req.body.jobNumber",req.body.jobNumber);
                          console.log("req.body.status",req.body.status);
                            jobs.update({  
                                    jobNumber: req.body.jobNumber, 
                                    "sentCarer.carerNo" : ""+req.body.carerNo+"",      
                                    }, {
                                        $set:{ 
                                            flag: "Completed",
                                            'sentCarer.$.carerJobstatus': 'stopped'
                                        }
                            },
                            function(err, data) {
                                if(err)
                                {
                                    res.json({
                                        success: false,
                                        message: internalError,
                                        data: err
                                    });
                                }   
                                else{
                                    if(req.body.status=='start')
                                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has STARTED Job #' + req.body.jobNumber]);
                                    else
                                        wamp.getSession().publish('app.carenet.jobs.admin', [ 'Carer ' + req.body.userName + ' has STOPPED Job #' + req.body.jobNumber]);
                                    res.json({
                                    success: true,
                                    message: req.body.jobNumber +'',
                                    data: job
                                    });    
                                }                
                            });                          
                      }


                      
                  }
    });
};


/**
Method Name: completeJob
Purpose: To complete the current job.
Created Date: 29th Nov 2016 3:00 PM
Created  By: Harish Mahajan
**/

exports.completeJob = function(req, res) {
    jobs.update({
        jobNumber: req.body.jobNumber
    }, {
        $set: {
            flag: "Completed"
        }
    }, function(err, job) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: jobCompleted,
                data: job
            });
        }

    });
};

/**
Method Name: deleteJob
Purpose: To delete the current job.
Created Date: 1 DEC 2016 2:00 PM
Created  By: Harish Mahajan
**/

exports.deleteJob = function(req,res){
    //console.log("Job deleted successfully...!!!");
    jobs.remove({jobNumber:req.body.jobNumber},function(err,job){
        if(err){
            res.json({
                success: false,
                message: internalError,
                data: err
            })
        }
        else{
            res.json({
                success:true,
                message: jobDeleted,
                data:job
            })
        }
    })
};

/**
Method Name: findJob
Purpose: To search job.
Created Date: 1 DEC 2016 2:00 PM
Created  By: Harish Mahajan
**/

exports.findJob = function(req, res) {
    var v = req.body.clientName;
    jobs.find({"clientName":{'$regex': v}}, function(err, list) {
        if (err) {
            res.json({
                success: false,
                message: noJobfound,
                data: err
            });
        } else {
            res.json({
                success: true,
                message: listOfjob,
                data: list
            });
        }

    });
};

/**
Method Name: carersjobList
Purpose: To search job which is assign to carer.
Created Date: 9 DEC 2016 11:16 AM
Created  By: Harish Mahajan
**/

exports.carersjobList = function(req, res) {
    carersjob.find({
        carerno: req.body.carerNo,
    }, function(err, list) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
            if(list.length!=0)
            {
                jobs.find({
                    jobNumber: {$in : list[0].jobno},
                    jobStatus: { $nin: 'stop' }
                }, function(err, joblist) {
                    if (err) {
                        res.json({
                            success: false,
                            message: noJobfound,
                            data: err
                        });
                    }
                    else
                    {
                        res.json({
                            success: true,
                            message: listOfjob,
                            data: joblist
                        });
                    }
                });
            }
            else{
                res.json({
                            success: false,
                            message: noJobfound,
                            data: list.length
                        });
            }
        }
    });
};

/**
Method Name: carerspastjobList
Purpose: To search past job which is assign to carer.
Created Date: 9 DEC 2016 11:16 AM
Created  By: Harish Mahajan
**/

exports.carerspastjobList = function(req, res) {
    carersjob.find({
        carerno: req.body.carerNo,
    }, function(err, list) {
        if (err) {
            res.json({
                success: false,
                message: internalError,
                data: err
            });
        } else {
            if(list.length!=0)
            {
                jobs.find({
                    jobNumber: {$in : list[0].jobno},
                    jobStatus: { $in: 'stop' }
                }, function(err, joblist) {
                    if (err) {
                        res.json({
                            success: false,
                            message: noJobfound,
                            data: err
                        });
                    }
                    else
                    {
                        res.json({
                            success: true,
                            message: listOfjob,
                            data: joblist
                        });
                    }
                });
            }
            else
            {
                res.json({
                            success: false,
                            message: noJobfound,
                            data: list.length
                        });
            }
        }
    });
};