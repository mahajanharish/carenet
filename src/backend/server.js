var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var logger = require('morgan');
var expressValidator = require('express-validator');
var config = require('./config/config');

var wamp = require('./controller/wamp');
var router = express.Router();
var apiRoutes = express.Router();

  var server = require('http').createServer(app);
//  var io = require('socket.io')(server);

// io.on('connection', (socket) => {
//   console.log('user connected');
//   socket.on('disconnect', function(){
//     console.log('user disconnected');
//   });

//   socket.on('add-message', (message) => {
//     console.log('message Recived ', message);
//     io.emit('message', {type:'new-message', text: message});
//   });
// });

//-------> /etc/nginx/cert.crt

var jwt = require("jsonwebtoken");
var users = require('./controller/users.js');
var job = require('./controller/job.js');
// var socket = require('./controller/wamp.js');
var session = require('express-session');
var cors = require('cors');
// var socketio = require("socket.io");
var cmd = require('node-cmd');
mongoose.connect(config.database);
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));
// cmd.run('crossbar start');
// cmd.get(
//  'crossbar status',
//  function(data){
//    // console.log(data);
//      console.log('Crossbar started:',data)
//  }
// );
app.use(expressValidator());
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'carenet'
}));

router.post('/user', users.create);
router.post('/login', users.login);
apiRoutes.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['token'];
    if (token) {
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.',
                    data: err
                });
            } else {
                req.user = decoded._doc;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.',
            data: null
        });
    }
});

app.use('/api', apiRoutes);
router.post('/api/job',job.create);
router.post('/api/list',job.list);
router.post('/api/listjob',job.listJob);
router.post('/api/carersjoblist',job.carersjobList);
router.post('/api/carerspastjoblist',job.carerspastjobList);
router.post('/api/update',job.update);
router.post('/api/history',job.history);
router.post('/api/jobtocarer',job.jobToCarer);
router.post('/api/carerlist',users.list);
router.post('/api/selectedcarerlist',job.selectedcarerlist);
router.post('/api/acceptjob',job.acceptJob);
router.post('/api/rejectjob',job.rejectJob);
router.post('/api/carerjoblist',job.carerjoblist);
router.post('/api/updateAction',job.updateAction);
router.post('/api/jobnote',job.jobNote);
router.post('/api/completejob',job.completeJob);
router.post('/api/deletejob',job.deleteJob);
router.post('/api/findjob',job.findJob);
router.post('/test',function(req,res){
    console.log("notify........");
    console.log(req.body);
    res.json(true);
});
app.use('/', router);
process.on('uncaughtException', function(err) {
    console.log('Caught exception: ' + err);
    console.log(err.stack);
});

server.listen(3001);
console.log("started on 3001");
