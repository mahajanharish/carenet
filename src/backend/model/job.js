var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb://localhost:27017/carenet");
autoIncrement.initialize(connection);
var jobSchema = new mongoose.Schema({
  sentCarer: [{
    carerNo: String,
    carerJobstatus:String
  }],
  clientName: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  dob: {
    type: String,
    required: false
  },
  alerts: {
    type: String,
    required: false
  },
  date_and_time_care_episode_required : {
    type: String,
    required: false
  },
  address: {
    type: String,
    required: true
  },
  telephone: {
    type: String,
    required: true
  },
  nok: {
    type: String,
    required: true
  },
  careNeeds: {
    type: String,
    required: true
  },
  jobDate: {
    type: String,
    required: true
  },
  flag: {
    type: String,
    required: false
  },
  jobAssigned: {
    type: String,
    required: false
  },
  notes: {
    type: String,
    required: false
  },
  jobStatus: {
    type: String,
    required: false
  },
  jobDetails: {
    type: String,
    required: false
  },
  rejectedCarer: [{
    carerNo: String,
  }],
  jobLog: [{
    message: String,
    time: String,
  }]

});

jobSchema.plugin(autoIncrement.plugin, {
  model: 'job',
  field: 'jobNumber',
  startAt: 1000,
  incrementBy: 1
});
jobSchema.plugin(autoIncrement.plugin, {
  model: 'job',
  field: 'clientNumber',
  startAt: 1000,
  incrementBy: 1
});
module.exports = mongoose.model('job', jobSchema);
