var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb://localhost:27017/carenet");
autoIncrement.initialize(connection);
var UserSchema = new mongoose.Schema({

    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    fullName: {
        type: String,
        required: true
    },
    userType: {
        type: String,
        required: true
    },
    mobileNo: {
        type: String,
        required: true
    }
});

UserSchema.plugin(autoIncrement.plugin, {
    model: 'user',
    field: 'userId',
    startAt: 1,
    incrementBy: 1
});
UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
    if (this.password) {
        return bcrypt.compareSync(password, this.password);
    } else {
        return false;
    }
};
// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
