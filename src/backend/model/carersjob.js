var mongoose = require('mongoose');
var connection = mongoose.createConnection("mongodb://localhost:27017/carenet");
var CarerSchema = new mongoose.Schema({
    jobno:  [{
    type: String,
  }],
    carerno: {
        type: String
    },
    status: {
        type: String
    }
});
// Export the Mongoose model
module.exports = mongoose.model('carer', CarerSchema);
