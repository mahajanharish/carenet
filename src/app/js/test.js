    $(document).ready(function(){
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            //format: 'dd/mm/yyyy HH:mm',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
       	$('.form_date').datetimepicker({
            // language:  'fr',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
        
      });