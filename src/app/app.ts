import {Component, Output} from '@angular/core';
import {Router} from '@angular/router'
import {Home} from './home/home';
//export var urls: string="http://carenet.chiasma.com.au:3001";
//export var urls: string="http://103.11.55.81:3001/backend";

//export var urls: string="/backend";  //Sever URL
export var urls: string="http://localhost:3001";  // Local URL
@Component({
  selector   : 'app',
  templateUrl: './app.html',
  styles: [`
  .log{
    display:none;
  }
  .logos{
    display:none;
  }`],
})
export class AppComponent {
  public islogin=false;
  childVlaue=false;
  constructor(private router: Router ){
    if(localStorage.getItem("token"))
      this.islogin = true;
  }
  public signout()
  {
    localStorage.removeItem("token");
    localStorage.removeItem("userType");
    this.router.navigate(['home']);
  }
}
