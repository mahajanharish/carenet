
import {Component} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Topheader} from '../topheader/topheader';
import { NotificationService }  from '../notification/notificationService';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import myGlobals = require('../app');
import * as moment from 'moment';

@Component({
  selector: 'jobs',
  styleUrls: ['./jobs.css'],
  templateUrl: './jobs.html',
  providers: [NotificationService]
})
export class Jobs {

  // Variable declaration
  public url=myGlobals.urls;
  public careData   : any;
  public carersjob:string[];
  public ifNotify : boolean;
  public notificationMsg : String;
  public notificationId : any;
  public screen : String;
  innerHeight: any;
  public id: any;
  public age        : number;
  public gender     : String;
  public post       : any;
  public address    : any;
  public clientName : String;
  public careNeeds  : String;
  public acceptedJob: boolean;
  public jobStatus  : boolean;
  public jobState   : String;
  public message    : string;
  public connection : any;
  public jobnumber  : any;
  public carerspastjob:string[];
  loadAPI: Promise < any > ;
  public lat: number;
  public lng: number;
  public showMsg:boolean;
  public showincomingMsg:boolean;
  public jobDate:any;
  public jobTime:any;
  public alerts:any;
  public date_and_time_care_episode_required:any;

  // When page loads, first contructor will call
  constructor(private router:Router, private http:Http, private ar: ActivatedRoute, private notify:NotificationService){
    var checkToken=localStorage.getItem("token");
    this.innerHeight = (window.innerHeight - 160 ) + "px";
    this.joblists();
    this.pastjoblists();
    this.screen = "overview";
    var myParent = this;
    if(checkToken === null)
    {
      this.router.navigate(['home']);
    }
    else
    {
        this.router.navigate(['jobs']);
    }
    function reloadData(data){
      //console.log(data);
      //console.log(this);
      myParent.showincomingMsg=false;
      myParent.joblists();
      myParent.pastjoblists();
      setTimeout(() => {
          myParent.notificationId = data;
          myParent.ifNotify = true;
          myParent.notificationMsg = "You have assign new job #" + data;
          setTimeout(() => {
              myParent.ifNotify = false;
              myParent.notificationMsg = null;
          }, 10000);
      }, 2000);
    }

  notify.connect().then(function(data) {
      // demo heartbeat channel
      // would be received when socket init
      // backend publish event on server start
      // frontend subscribe event on app init
      var session = data.session;
      var connection = data.connection;

      var channel = "app.carenet.jobs." + localStorage.getItem("carerNo");
      //console.log("subscribe to " + channel);
      session.subscribe(channel,reloadData);

      connection.onclose = function(reason, details) {
          //console.log("disconnect in joblist");
          session.unsubscribe(channel);
      };

      connection.onopen = function(session) {
         // console.log("connect in joblist");
         session.subscribe(channel, reloadData);
         // console.log("subscribe to " + channel);
      };

    });
  }

  // Added By Harish
  // This function is use to display all the jobs which is assign to user by admin
  joblists()
  {
    var joblistsUrl= this.url + "/api/carersjoblist";
    var token = localStorage.getItem("token");
    var carerNo= localStorage.getItem("carerNo");
    var postdata={'token':token,'carerNo':carerNo};
    this.http.post(joblistsUrl,postdata).map( res =>res.json()).subscribe(data => {
    this.carersjob = data.data;
    if(data.data == 0)
      this.showincomingMsg = true;
    //for(i=0;i<data.data.length;i++)
    //{
       // console.log(data.data[i].clientName);
    //}
    });
  }

  // Added By Harish
  // This function is use to display all the completed jobs by carer
  pastjoblists()
    {
      var joblistsUrl= this.url + "/api/carerspastjoblist";
      var token = localStorage.getItem("token");
      var carerNo= localStorage.getItem("carerNo");
      var postdata={'token':token,'carerNo':carerNo};
      this.http.post(joblistsUrl,postdata).map( res =>res.json()).subscribe(data => {
      this.carerspastjob = data.data;
      if(data.data == 0)
        this.showMsg = true;
      });
    }

  // Added By Harish
  toggleScreen(){
    this.joblists();
    this.pastjoblists();
    if(this.screen==="detail"){
      this.screen="overview";
    }
  }

  // FOR DETAIL SCREEN
  jobdetails(jobDetails)
  {
    this.screen = "detail";
    this.id = jobDetails.jobNumber;
    this.gender = jobDetails.gender;
    this.clientName = jobDetails.clientName;
    this.address = jobDetails.address;
    this.age = moment().diff(moment(jobDetails.dob, 'DDMMYYYY'), 'years');
    this.jobState = jobDetails.jobStatus;
    this.jobStatus = false;
    this.alerts = jobDetails.alerts;
    this.date_and_time_care_episode_required = jobDetails.date_and_time_care_episode_required;

    // Added By Harish    
    this.http.get("https://maps.google.com/maps/api/geocode/json?address="+jobDetails.address+"&sensor=false").map(res=>res.json()).subscribe(data=>{
      this.lat=data.results[0].geometry.location.lat;
      this.lng=data.results[0].geometry.location.lng;
    });
    
    if(jobDetails.jobAssigned){
        this.acceptedJob = true;
    }
    if(jobDetails.jobStatus === "start"){
      this.jobStatus = true;
    }
    else if(jobDetails.jobStatus === "stop"){
      this.acceptedJob = false;
    }
    this.careNeeds = jobDetails.careNeeds;
  }

  // When click on notification then this function will redirect to you  on job details page
  notificationClickjobdetails(jobno)
  {
       this.post = {
          'token': localStorage.getItem("token"),
          'jobNumber': jobno
        };
        this.http.post(this.url + "/api/history", this.post).map(res => res.json()).subscribe(data => {
           this.careData = data.data;
           this.jobState = this.careData.jobStatus;
           if(this.careData.jobAssigned){
               this.acceptedJob = true;
           }
          
           if(this.careData.jobStatus === "start"){
             this.jobStatus = true;
           }
           else if(this.careData.jobStatus === "stop"){
             this.acceptedJob = false;
           }


           this.id = jobno;
           this.careNeeds = this.careData.careNeeds;
           this.gender = this.careData.gender;
           this.address = this.careData.address;
           this.age = moment().diff(moment(this.careData.dob, 'DDMMYYYY'), 'years');
           this.screen = "detail";
           this.clientName = this.careData.clientName;
           this.jobState = this.careData.jobStatus;

           this.http.get("https://maps.google.com/maps/api/geocode/json?address="+this.careData.address+"&sensor=false").map(res=>res.json()).subscribe(datas=>{
              this.lat=datas.results[0].geometry.location.lat;
              this.lng=datas.results[0].geometry.location.lng;
           });           

        });
  }

  // To acccept the job
  // Added By Harish
  onAcceptclick(id) {
    var jobassignUrl = this.url + "/api/acceptjob";
    var token = localStorage.getItem("token");
    var carerno = localStorage.getItem("carerNo");
    var postdata = {
      'token': token,
      'carerNo': carerno,
      'userName': localStorage.getItem('uname'),
      'jobNumber': id
    };
    this.http.post(jobassignUrl, postdata).map(res => res.json()).subscribe(data => {
      if(data.data.ok){
        this.acceptedJob = true;
        this.jobState = "accept";
        this.message=  this.id;
      }

    });
  }

  // To decline the job
  // Added By Harish
  onDeclineclick(id) {    
    var jobassignUrl = this.url + "/api/rejectjob";
    var token = localStorage.getItem("token");
    var carerno = localStorage.getItem("carerNo");
    var postdata = {
      'token': token,
      'carerNo': carerno,
      'userName': localStorage.getItem('uname'),
      'jobNumber': id
    };
    this.http.post(jobassignUrl, postdata).map(res => res.json()).subscribe(data => {
      this.jobState = "decline";
    });
  }

    // for starting or ending the job
  action(status){
      var actionUpdateUrl = this.url + "/api/updateAction";
      var token = localStorage.getItem("token");
      var carerno = localStorage.getItem("carerNo");
      var  updateData= {
        'token': token,
        'status': status,
        'carerNo': carerno,
        'userName': localStorage.getItem('uname'),
        'jobNumber': this.id
      };

      this.http.post(actionUpdateUrl,updateData).map(res => res.json()).subscribe(data => {
        if(status != 'start'){
            this.screen = "overview";
        }
        this.joblists();
        this.pastjoblists();
      });

      if(status == 'start'){
        this.jobStatus = true;
        this.jobState = "Start";
      }
      else{
        this.jobStatus = false;
        this.acceptedJob = false;
        this.jobState = "Stop";
        this.showMsg = false;
      }
    }

   convertDobToAge(dob) {
      return moment().diff(moment(dob, 'DDMMYYYY'), 'years');
    }

   add_hours (dt,flag) { 
      var dates = moment(dt);
      if(flag==0) 
        return moment(dates).format("HH:mm"); // Displays actual time
      else
        return moment(dates).add(2, 'hours').format("HH:mm"); // Add two hours to actual time
    }  

    getDate(dt) { 
      return moment(dt).format("DD-MM-YYYY");      
    } 
}
