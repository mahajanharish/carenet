
import {  Component,
  Input,
  trigger,
  state,
  style,
  transition,
  animate,
  EventEmitter
  } from '@angular/core';

//import { FormBuilder, Validators} from "@angular/common";
import {Router} from '@angular/router';
import * as moment from 'moment';
//import {AppComponent} from '../app';
//import myGlobals = require('../app');

@Component({
  selector: 'topheader',
  styleUrls:['./topheader.css'],
  templateUrl: './topheader.html',
})

export class Topheader {
  public loginuser;
  public userType;
  public islogin=false;
  public today;
  public dd;

  //public islogin=localStorage.getItem("loginusers");

  constructor(private router: Router ){
    this.today =  moment().format("MMM Do YYYY, HH:mm");
    //this.dd = this.today + " " + new Date().getHours() + ":" + new Date().getMinutes();
    if(localStorage.getItem("token"))
    {
      this.islogin = true;
      //if(localStorage.getItem("uname")!=null)
      this.loginuser = localStorage.getItem("uname");
      this.userType =  localStorage.getItem("userType");
    }
  }
  public signout()
  {
    //this.islogin=false;
    //localStorage.removeItem("loginusers");
    localStorage.removeItem("token");
    localStorage.removeItem("userType");
    localStorage.removeItem("uname");
    localStorage.removeItem("carerNo");
    this.router.navigate(['home']);
    //location.reload();
  }
  }
