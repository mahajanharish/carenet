import {NgModule} from '@angular/core'
import {RouterModule} from "@angular/router";
import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app";
//import {Github} from "./github/shared/github";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule, JsonpModule} from "@angular/http";

//import {About} from './about/about';
import {Home} from './home/home';
import {Jobs} from './jobs/jobs';
//import {Job2} from './job2/job2';
//import {Jobdetails} from './jobdetails/jobdetails';
import {Dashboard} from './dashboard/dashboard';
import {Createjob} from './createjob/createjob';
import {Topheader} from './topheader/topheader';
import {Ng2Bs3ModalModule} from 'ng2-bs3-modal/ng2-bs3-modal';
//import {RepoBrowser} from './github/repo-browser/repo-browser';
//import {RepoList} from './github/repo-list/repo-list';
//import {RepoDetail} from './github/repo-detail/repo-detail';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  declarations: [AppComponent, Home, Jobs, Topheader, Dashboard, Createjob],
  imports     : [BrowserModule, FormsModule, ReactiveFormsModule, HttpModule, JsonpModule, RouterModule.forRoot(rootRouterConfig),Ng2Bs3ModalModule, AgmCoreModule.forRoot({apiKey: 'AIzaSyAYzEmUSSsz3uCMVN_gpJ_6DMUji2xFQA0'})],
  providers   : [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap   : [AppComponent]
})
export class AppModule {

}
