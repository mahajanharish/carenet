//import {Component, EventEmitter} from '@angular/core';
import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Pipe,
  PipeTransform
} from '@angular/core';
// import {custompipe} from './custompipe';
import {
  ModalComponent
} from 'ng2-bs3-modal/ng2-bs3-modal';
import {
  Router
} from '@angular/router';
//import {AppComponent} from '../app';
import {
  Http,
  Headers,
  Response,
  RequestOptions
} from '@angular/http';
import * as moment from 'moment';
import {
  NotificationService
} from '../notification/notificationService';
//import * as jquery from 'jquery';
//import {Topheader} from '../topheader/topheader';
import myGlobals = require('../app');

export class Jobs {

  id: number;
  clientName: string;
  jobNumber: number;
  careNeeds: string;
  telephone: number;
  address: string;
  gender: string;
  nok: string;
  jobLog: string;
  message: string;
  time: Date;
  sentCarer:string;
  //carerNo:number;
}


@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.css'],
  templateUrl: './dashboard.html',
  providers: [NotificationService]
    // pipes : [custompipe]
})
export class Dashboard {
  @ViewChild('carers')
  modal: ModalComponent;
  public url = myGlobals.urls;
  public jobarray: string[];
  public carerarray: string[];
  public carerarray1: string[];
  public joblogMessage: string;
  public selectDiv: number;
  public carerNo: number;
  public ifNotify : boolean;
  public notificationMsg : String;
  selectedJobs: Jobs;
  public changesize: any;
  innerHeight: any;
  public jobid;
  public chk_carerno;
  public showMsg:boolean;
  public setglobal:boolean;
  public isjobsend:any;
  public flag:any;
  public temp:string[];
  public setstylecolor:any;
  public popup:any;
  public rowHight:any;
  public toprowheight:any;
  public connection : any;
  //public isjoblogs : any;
  public CurrentDate : any;
  public num:any;
  public carerNoJobstatus:any;
  public carerjobState:string;
  public triangleShow:boolean;
  public defaultJobSelected:boolean;

  constructor(private router: Router, private http: Http, private notify: NotificationService) {

    this.joblists();
    this.carerlists();
    this.toprowheight = 60;
    this.innerHeight = (window.innerHeight - 160) + "px";
    this.rowHight = (window.innerHeight - 160 - this.toprowheight) / 6 + "px";
    this.toprowheight = this.toprowheight + "px";
    this.ifNotify = false;
    this.triangleShow = true;
    this.defaultJobSelected=false;
    //this.isjoblogs = false;
    var checkToken = localStorage.getItem("token");
    var checkUserType = localStorage.getItem("userType");
    var myParent = this;

    if (checkToken === null) {
      this.router.navigate(['home']);
    } else {
      if (checkUserType === "admin") {

        this.router.navigate(['dashboard']);
      } else {
        this.router.navigate(['jobs']);
      }
    }

    // This function is use for refresh or reload the data & send notification
    function reloadData(data){
        myParent.joblistsrefreshonly();
        setTimeout(() => {
          myParent.ifNotify = true;
          myParent.notificationMsg = data;
          setTimeout(() => {
              myParent.ifNotify = false;
              myParent.notificationMsg = null;
          }, 10000);
      }, 2000); 
    }

    notify.connect().then(function(data) {
      // demo heartbeat channel
      // would be received when socket init
      // backend publish event on server start
      // frontend subscribe event on app init
      var session = data.session;
      var connection = data.connection;

      var channel = "app.carenet.jobs.admin";
      //console.log("subscribe to " + channel);
      session.subscribe(channel,reloadData);


      connection.onclose = function(reason, details) {
        //console.log("disconnect in joblist");
          session.unsubscribe(channel);
      };

      connection.onopen = function(session) {
        //console.log("connect in joblist");
          session.subscribe(channel, reloadData);
          //console.log("subscribe to " + channel);
      };

    });

  }
  selectedCarer = [];
  selectedCarerName = [];
  sentcarerlist = [];
  newarray = [];
  carerflag = [];

  onLoadCarer(job: Jobs): void {
    var carerlistsUrl = this.url + "/api/carerlist";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token
    };
    this.http.post(carerlistsUrl, postdata).map(res => res.json()).subscribe(data => {
      var i;
      //console.log("onloadJobnumber :- ",job['jobNumber']);
      this.carerarray1 = data.data; 
      this.sentcarerlist = [];
      this.changesize = "60px";
      this.selectDiv = job['jobNumber'];
      this.chk_carerno = "";
      this.jobid = job['jobNumber'];
      this.setglobal = false;
      var carerjobState = "";
      let newarray1:any;		     
      newarray1=job['sentCarer'];
      //var newarray1=job['sentCarer'];
      var carerflagNew = [];
      this.carerarray1.forEach(function(totalcarer) {
      var flags = false,
      count = 0;
        newarray1.forEach(function(carers) {
          count++;
          if (totalcarer['userId'] == carers.carerNo) {
            flags = true;
          }
          // For carer's job status
        let sentcarer : any;		          
        sentcarer = job.sentCarer;		
        sentcarer.forEach(function(state) {
            if((totalcarer['userId']+"")===state.carerNo)
            {
              carerjobState = state.carerJobstatus;
            }
          });

          // For carer's information
          if (count == newarray1.length)
          {
             carerflagNew.push({
              userId: totalcarer['userId'],
              flag: flags,
              fullName: totalcarer['fullName'],
              mobileNo: totalcarer['mobileNo'],
              userType: totalcarer['userType'],
              email: totalcarer['email'],
              carerJobstatus: carerjobState,
              ids: totalcarer['_id']
            });
             carerjobState = "";
          }
        });
      });

      if(carerflagNew.length > 0)
      {
        this.flag=true;
        this.carerarray=carerflagNew;
      }
      else{
        this.flag=false;
        this.carerlists();
      }
    });
    this.defaultJobSelected=false;
  }

  // To remove the job
  // Added by Harish
  completejob(id) {
    var completejobUrl = this.url + "/api/completejob";
    var token = localStorage.getItem("token");
    //var carerno = localStorage.getItem("carerNo");
    var postdata = {
      'token': token,
      'jobNumber': id
    };
    this.http.post(completejobUrl, postdata).map(res => res.json()).subscribe(data => {
      this.modal.dismiss();
      this.joblists();
    });
  }

  onSelect(job: Jobs): void {
    this.sentcarerlist = [];
    this.changesize = "60px";
    this.selectDiv = job['jobNumber'];
    this.chk_carerno = "";
    this.jobid = job['jobNumber'];
    this.setglobal = false;
    let newarray : any;		    
    newarray = job['sentCarer'];
    //var newarray = job['sentCarer'];
    var carerflagNew = [];
    var statusCounter=0;
    var carerjobState = "";
    //console.log("this.selectDiv",this.selectDiv);
    this.carerarray.forEach(function(totalcarer) {
      var flags = false,
      count = 0;
      newarray.forEach(function(carers) {
        count++;
        if (totalcarer['userId'] == carers.carerNo) {
          flags = true;
        }

        if (count == newarray.length)
        {
          // For carer's job status
          let sentcarer : any;		         
          sentcarer = job.sentCarer;		
          sentcarer.forEach(function(state) {
            if((totalcarer['userId']+"")===state.carerNo)
            {
              carerjobState = state.carerJobstatus;
            }
          });

          // For carer's information
          carerflagNew.push({
            userId: totalcarer['userId'],
            flag: flags,
            fullName: totalcarer['fullName'],
            mobileNo: totalcarer['mobileNo'],
            userType: totalcarer['userType'],
            email: totalcarer['email'],
            carerJobstatus: carerjobState,
            ids: totalcarer['_id']
          });
          carerjobState = "";
        }
      });
    });
    if(carerflagNew.length > 0)
    {
      this.flag=true;
      this.carerarray=carerflagNew;
    }
    else{
      this.flag=false;
      this.carerlists();
    }
    
    this.selectedCarer = [];
    this.selectedCarerName = [];
    this.selectedJobs = job;
    this.joblogMessage = job.jobLog;
    this.triangleShow=false;
  }

  getminutes(dt) {
    var temp = "";
    let now = moment().format("YYYY-MM-DD hh:mm:ss a");
    let then = moment(dt).format("YYYY-MM-DD hh:mm:ss a");
    var startTime = moment(then, 'YYYY-MM-DD hh:mm:ss a');
    var endTime = moment(now, 'YYYY-MM-DD hh:mm:ss a');
    var weeks = endTime.diff(startTime, 'weeks');
    var diffDays = endTime.diff(startTime, 'days');
    var diffHours = endTime.diff(startTime, 'hours');
    var min = endTime.diff(startTime, 'minutes');
    var diffSeconds = endTime.diff(startTime, 'seconds');
    this.getStyle(min, temp);
    // console.log("weeks--"+weeks+"diffDays-"+diffDays+"diffHours-"+diffHours+"diffMinutes-"+min+"diffSeconds-"+diffSeconds);
    if (diffSeconds <= 59) {
      return diffSeconds + " Seconds ago";
    } else if (diffSeconds <= 3539) {
      return min + " Minutes ago";
    } else if (diffSeconds <= 86399) {
      return diffHours + " Hours ago";
    } else {
      return diffDays + " Days ago";
    }
  }

  onChange(flag, jobnumber, carerno,name) {
    if (flag == true) {
      this.selectedCarer.push(carerno);
      this.selectedCarerName.push(name);
    }
    if (flag == false) {
      var index = this.selectedCarer.findIndex(function(o) {
        return o === carerno;
      })
      this.selectedCarer.splice(index, 1);
      var index12 = this.selectedCarerName.findIndex(function(o) {
        return o === name;
      })
      this.selectedCarerName.splice(index12, 1);
    }
  }

  sentcarer(list,jobnumber,carercheck)
  {
    if(list.length > 0)
    {
      this.popup='sendjob';
      this.modal.open();
    }
    else
    {
     this.popup='selectcarer';
     this.modal.open();
    }
  }

  // To remove the job
  // Added by Harish
  deleteJob(jobid){
      var deletejoburl = this.url + "/api/deletejob";
      var token = localStorage.getItem("token");
      var postdata = {"token":token,"jobNumber":jobid};
      this.http.post(deletejoburl,postdata).map(res=>res.json()).subscribe(data=>{
        this.modal.dismiss();
        this.joblists();
    });
  }
  
  modelpopup(status,no)
  {
    this.popup=status;
    this.num=no;
    this.modal.open();
  }

  jobsenttocarer(list, jobnumber) {
    this.modal.close();
    var jobassignUrl = this.url + "/api/jobtocarer";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token,
      'carerNo': list,
      'jobNumber': jobnumber,
      'carerJobstatus':'Sent'
    };

    this.http.post(jobassignUrl, postdata).map(res => res.json()).subscribe(data => {
       this.joblists();
       this.selectedcarerlists(jobnumber);
       this.triangleShow = false;
    });
  }

  getStyle(min, flags) {
    if (min > 60 && flags === 'Unallocated') {
      // Red
      return "#F9B1B1";
      // return "#FFE2CA";
    } else if (min > 30 && min <= 60 && flags === 'Unallocated') {
      return "#FCD78D";
      //Orange
    } else if (min > -59 && min <= 30 && flags === 'Unallocated') {
      return "#CCFFCC";
      //Green
    } else if (flags === "Allocated") {
      return "#FFFFFF";
      //White
    } else if (flags === "Completed") {
      this.setstylecolor = "Green";
      return "#d9d9d9";
      // #D3D3D3
      // Gray
    }
  }

  // To Search the job, this is use for filter
  // Added By Harish
  eventHandler(event,serchitem)
  {
    if(event.keyCode===13)
    {
      this.showMsg=false;
      var findjobUrl=this.url + "/api/findjob";
      var token = localStorage.getItem("token");
      var postdata = {'token':token,'clientName':serchitem};
      this.http.post(findjobUrl, postdata).map(res=>res.json()).subscribe(data=>{
      var i;
      this.jobarray = data.data;
      if(data.data.length==0)
      {
        this.showMsg=true;
      }

      this.jobid = data.data[0].jobNumber;
      this.selectDiv = data.data[0].jobNumber;
      this.changesize = "60px";
      this.selectedJobs = data.data[0];
      this.setglobal = false;
      this.joblogMessage = data.data[0].jobLog;
      this.isjobsend = data.data[0].sentCarer.length;

      if(this.isjobsend > 0)
      {
        this.flag=true;
        this.setglobal = true;
      }
      else{
        this.setglobal = false;
        this.flag=false;
      }
      })
    }
  }

  convettoDatetime(d) {
    if (d != undefined)
      return moment(d).format("DD/MM/YYYY hh:mm a");
  }

  // To get the list of all Jobs
  // Added By Harish
  joblists() {
    
    var joblistsUrl = this.url + "/api/list";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token
    };
    var flag;
    
    this.http.post(joblistsUrl, postdata).map(res => res.json()).subscribe(data => {
    var i;
    this.jobarray = data.data;
    this.isjobsend = data.data[0].sentCarer[0].carerNo;
    this.temp = data.data[0].sentCarer;
    if (this.isjobsend > 0) {
      this.temp = data.data[0].sentCarer;
      this.setglobal = true;
    } else {
      this.setglobal = false;
    }

    this.jobid = data.data[0].jobNumber;
    this.selectDiv = data.data[0].jobNumber;
    this.changesize = "60px";
    this.selectedJobs = data.data[0];
    this.setglobal = false;
    this.joblogMessage = data.data[0].jobLog;
    this.isjobsend = data.data[0].sentCarer.length;
    if(this.isjobsend > 0)
    {
      this.flag=true;
      this.setglobal = true;
    }
    else{
      this.setglobal = false;
      this.flag=false;
    }
    if(this.defaultJobSelected==true)
      this.onLoadCarer(data.data[0]);
    });
  }

  // When carer accepted/declined/started/stopped job then below function is used for refresh the data
  // Added By Harish
  joblistsrefreshonly() {
    var joblistsUrl = this.url + "/api/list";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token
    };
    
    this.http.post(joblistsUrl, postdata).map(res => res.json()).subscribe(data => {
      this.jobarray = data.data;
      this.selectedcarerlists(this.selectDiv);
    });
  }

  // To get the list of all carers
  // Added By Harish
  carerlists() {
    var carerlistsUrl = this.url + "/api/carerlist";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token
    };
    this.http.post(carerlistsUrl, postdata).map(res => res.json()).subscribe(data => {
      this.carerarray = data.data; 
    });
  }

  // This function is use to selected carer's lists
  selectedcarerlists(jobnum) {
    var carerlistsUrl = this.url + "/api/selectedcarerlist";
    var token = localStorage.getItem("token");
    var postdata = {
      'token': token,
      'jobNumber':jobnum
    };
    this.http.post(carerlistsUrl, postdata).map(res => res.json()).subscribe(data => {
      var i;
      this.onSelect(data.data);
      this.triangleShow = false;
    });
  }

  convertDobToAge(dob) {
    return moment().diff(moment(dob, 'DDMMYYYY'), 'years');
  }

  getjobtime(date) {
    let now = moment().format("YYYY-MM-DD hh:mm:ss a");
    let then = moment(date).format("YYYY-MM-DD hh:mm:ss a");
    var startTime = moment(then, 'YYYY-MM-DD hh:mm:ss a');
    var endTime = moment(now, 'YYYY-MM-DD hh:mm:ss a');
    var min = endTime.diff(startTime, 'minutes');
    return min;
  }

}