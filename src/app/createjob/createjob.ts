import {Component,AfterViewInit,ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Http,Headers,RequestOptions,Response} from '@angular/http';
import myGlobals = require('../app');
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import { NotificationService }  from '../notification/notificationService';

@Component({
    selector: 'createjob',
    styleUrls: ['./createjob.css'],
    templateUrl: './createjob.html',
    providers: [NotificationService]
})

export class Createjob {
    
    // Variable Declarations
    @ViewChild('myModal')
    modal: ModalComponent;
    public url = myGlobals.urls;
    public notificationMsg : String;
    loadAPI: Promise < any > ;
    myForm: FormGroup;
    public jobCreatedmsg:any = "Job successfully created"
    
    constructor(private http: Http, private router: Router, fb: FormBuilder) {

        var token = localStorage.getItem("token");
        if (token) {
            this.buttonClicked();
            this.router.navigate(['/createjob']);
        } else {
            alert("Please Login.");
            this.router.navigate(['/home']);
        }

        this.myForm = fb.group({
            'clientName': [null, Validators.required],
            'gender': [null, Validators.required],
            // 'dob': [null, Validators.required],
            'address': [null, Validators.required],
            'telephone': [null, Validators.required],
            'name_of_next_of_kin': [null, Validators.required],
            'multiline_field_for_care_needs': [null, Validators.required],
            'alerts': [null, Validators.required],
            'jobDetails': [null, Validators.required]
        })

    }
    createjob(jobDate,date_and_time_care_episode_required,dob) {
        console.log(jobDate);
        var token = localStorage.getItem("token");
        if (token) {
            var createjobUrl = this.url + "/api/job";
            var flag = "Unallocated";
            var postdata = {
                'token': token,
                'clientName': jobDate.clientName,
                'gender': jobDate.gender,
                'dob': dob,
                'address': jobDate.address,
                'alerts': jobDate.alerts,
                'telephone': jobDate.telephone,
                'nok': jobDate.name_of_next_of_kin,
                'careNeeds': jobDate.multiline_field_for_care_needs,
                'date_and_time_care_episode_required': date_and_time_care_episode_required,
                'flag': flag,
                'jobStatus': 'Called',
                'jobDetails':jobDate.jobDetails
            };
            this.http.post(createjobUrl, postdata).map(res => res.json()).subscribe(data => {
                if (data.success){
                    this.modal.open();
                    //this.myForm.reset();
                }
            });
        } else {
            alert("Session Expired. Please Login again...");
        }
    }
    cleardata()
    {

    }
    public buttonClicked() {
        this.loadAPI = new Promise((resolve) => {
            this.loadScript();
        });
    }

    public loadScript() {
            let node = document.createElement('script');
            node.src = 'app/createjob/createjob.js';
            node.type = 'text/javascript';
            node.async = true;
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);
   }
}
