// window.onload = function ()
// {
 $(document).ready(function(){
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            format: 'yyyy-mm-ddThh:ii:ssZ',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1,
             pickerPosition: "top-left"
        });
       	$('.form_date').datetimepicker({
            // language:  'fr',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

      });
// };
