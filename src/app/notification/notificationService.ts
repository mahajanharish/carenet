import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import {Injectable} from "@angular/core";
var autobahn = require('autobahn');
// import myGlobals = require('../app');
import {Http} from '@angular/http';

@Injectable()
export class NotificationService {

  constructor(private http : Http) {
  };

  public connect(): Promise<any>  {
   return new Promise<any>((resolve, reject) => {
     // autobahn init connection
     // work independent to auth
     // setup in landing page of website
     // realm and url : Need to change at both end if you want to update anything
     var connection = new autobahn.Connection({
        //url: 'wss://localhost/socket/ws', 
        url: 'wss://carenet.chiasma.com.au/socket/ws',
        realm: 'realm1'
     });

     // listen for on CONNECT event
     connection.onopen = function(session) {
       //console.log("session connect");
       //console.log(session); 
        var mySessionData ={
          'connection': connection,
          'session': session
        }

        resolve(mySessionData);
          function onevent(args) {
            // alert(args);
            //console.log("Event:", args[0]);
          }
        // demo heartbeat channel
        // would be received when socket init
        // backend publish event on server start
        // frontend subscribe event on app init
        session.subscribe('app.carenet.heartbeat', onevent);
     };

     // listen for on CLOSE event
     connection.onclose = function(reason, details) {
       console.log("WAMP connection closed", reason, details);
     }
     // call on open to connect wamp
     connection.open();
   });

  }
}
