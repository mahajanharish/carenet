
import {Component,Input,trigger,
  state,
  style,
  transition,
  ViewChild,
  animate,
  EventEmitter,
  ElementRef,
  enableProdMode

  } from '@angular/core';
  enableProdMode();
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {Router} from '@angular/router';
import {Topheader} from '../topheader/topheader';
import 'rxjs/Rx';
//import {AppComponent} from '../app';
import myGlobals = require('../app');
import {
  ModalComponent
} from 'ng2-bs3-modal/ng2-bs3-modal';
declare var jQuery:any;

@Component({
  selector: 'home',
  styleUrls: ['./home.css'],
  templateUrl: './home.html',
  outputs: ['childChanged']
})

export class Home {
  @ViewChild('loginmodal')
  modal: ModalComponent;
  public ishidden1=true;
  public ishidden2=false;
  public url=myGlobals.urls;
  //public top : Topheader;
  childChanged = new EventEmitter<boolean>();

  constructor(private http:Http, private router: Router, private elementRef : ElementRef) {
    var checkToken=localStorage.getItem("token");
    if(checkToken)
    {
      this.router.navigate(['dashboard']);
    }
    else
    {
      this.router.navigate(['home']);
    }
   }


  onChange(value:boolean)
  {
     value=true;
     this.childChanged.emit(value);
   }
  
  login(event,username,userpassword) {
        var email = username;
        var password = userpassword;
        var loginUrl= this.url + "/login";
        var postdata={'email':email,'password':password};
        if(email!='' && password !='')
        {
          
          this.http.post(loginUrl,postdata)
          .map( res =>res.json()).subscribe(data => {
          var success = data.success;
          if(success)
          {
              localStorage.setItem("loginusers","true");
              localStorage.setItem("token",data.data);
              localStorage.setItem("userType",data.userType);
              localStorage.setItem("uname",data.user);
              localStorage.setItem("carerNo",data.userId);
              if(data.userType === "admin")
              {
                this.router.navigate(['dashboard']);
              }
              else
              {
                this.router.navigate(['jobs']);
              }
            }
            else
            {
               this.modal.open();
            }
          });
        }
  }

  // Added By Harish
  // To Register new carer 
  register(event,fullname, email, password, mobile){
      if(fullname != '' && email  != '' && password  != '' && mobile  != '')
      {
        var postdata={'fullName':fullname,'userType':'carer','email':email,'password':password,'mobileNo':mobile};
        this.http.post(this.url+"/user",postdata).map(res =>res.json()).subscribe(data=>{
            var success = data.success;
            if(success)
            {
              alert("User Added Successfully....!!!!");
            }
            else
            {
              alert("Invalid Data");
            }
        });
      }
  }

 // Added By Harish
 // To Hide and Show login popup and registration popup  
 clicked(event,v) {
   if(v==false)
   {
     this.ishidden1=false;
     this.ishidden2=true;
   }
   else
   {
     this.ishidden1=true;
     this.ishidden2=false;
   }
  }
}
