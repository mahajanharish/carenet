import {Routes} from '@angular/router';
import {Home} from './home/home';
import {Jobs} from './jobs/jobs';
import {Dashboard} from './dashboard/dashboard';
import {Createjob} from './createjob/createjob'
import {Topheader} from './topheader/topheader'
export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: Home},
  {path: 'jobs', component: Jobs},
  {path: 'dashboard', component: Dashboard},
  {path: 'createjob', component: Createjob},
  {path: 'topheader', component: Topheader}
];