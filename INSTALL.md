Install instructions go here

1) Make sure you have node.js installed version 5+

2) Make sure you have NPM installed version 3+

3) Database: Mongodb. To configure it use following steps
```
apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo "deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen" | tee -a /etc/apt/sources.list.d/10gen.list
apt-get -y update
apt-get -y install mongodb-10gen
```

4) WINDOWS ONLY run npm install -g webpack webpack-dev-server typescript to install global dependencies

5) run `npm install` to install dependencies

6) if you want to use other port, open package.json file, 
then change port in `--port 3002` script

7) To run this project we have to run backend first
```bash
cd carenet/src/backend
node server.js
```
now your backend is run on 3001 port

8) To run this project now we have to run frontend
```bash
cd carenet
npm start
```
now your frontend is run on 3002 port

9) open browser to http://domainname:3002

10) If you don’t want port in the end. Configure nginx and use it as reverse proxy

```nginx
server {
       listen 80;
       listen [::]:80;
       server_name carenet.chiasma.com.au;

       rewrite ^(.*) https://carenet.chiasma.com.au$1 permanent;

       location / {
        proxy_pass http://localhost:3002;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
     location ~* /backend/ {
        rewrite ^/backend(/.*)$ $1 break;
        proxy_pass http://localhost:3001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

       location ~* /sockjs* {
        rewrite https://localhost/sockjs-node(/.*)$ $1 break;
        proxy_pass http://localhost:3002;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

  location ~* /socket.* {
        proxy_pass http://localhost:3001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }
}

server {
    listen   443 ssl;
    server_name carenet.chiasma.com.au;

    ssl_certificate      /etc/letsencrypt/live/carenet.chiasma.com.au/fullchain.pem;
    ssl_certificate_key  /etc/letsencrypt/live/carenet.chiasma.com.au/privkey.pem;
    include /etc/nginx/snippets/ssl_include.conf;

    location / {
        proxy_pass http://localhost:3002;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

     location ~* /backend/ {
        rewrite ^/backend(/.*)$ $1 break;
        proxy_pass http://localhost:3001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

 location ~* /sockjs-node/ {
        rewrite https://carenet.chiasma.com.au/sockjs-node(/.*)$ $1 break;
        proxy_pass http://localhost:3002;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

     location ~* /socket.* {
        proxy_pass http://localhost:3001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

}

```

11) How to create Carer?
=> Click on "Create an account" link from login screen. 

12) Socket:- wamp is used for the socket

13) Crossbar:-

http://crossbar.io/docs/Installation-on-Ubuntu-and-Debian/

After crossbar installation goto your root directory

Now you can run following commands 

crossbar init -> this will create folder named as .crossbar
crossbar start -> will start the crossbar instance

Remember whenever you do the npm start and for backend node server.js
you also need to do above steps 
To configure everything 
